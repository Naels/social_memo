import React from 'react'

import { useSelector } from 'react-redux'
import { Grid, CircularProgress } from '@mui/material'

import Post from './Post/Post'

const Posts = ({setCurrentId, handleModalVisibility }) => {
  const posts = useSelector((state) => state.posts)

  return (
    !posts.length ? <CircularProgress/> :(
      <Grid style={{display: 'flex',alignItems: 'center'}} container alignItems="stretch" spacing={3}>
        {posts.map((post) => (
          <Grid key={post._id} item xs={12} sm={4}>
            <Post post={post} setCurrentId={setCurrentId} handleModalVisibility={handleModalVisibility}/>
          </Grid>
        ))}
      </Grid>
    )
  
  )
}

export default Posts