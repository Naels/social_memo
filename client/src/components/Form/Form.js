import React, { useState, useEffect } from "react";
import { TextField, Button, Typography, Paper } from "@mui/material";
import FileBase  from 'react-file-base64'

import { useDispatch } from "react-redux";
import { createPost, updatePost } from "../../actions/posts";
import { useSelector } from 'react-redux'

const Form = ({ currentId, setCurrentId, handleModalVisibility }) => {
  const [postData, setPostData] = useState({
    title: "",
    message: "",
    tags: "",
    selectedFile: "",
  });

  const currentUser = JSON.parse(localStorage.getItem('profile'))

  const post = useSelector((state) => currentId ? state.posts.find((p) => p._id === currentId) : null)
  const dispatch = useDispatch()

  useEffect(()=>{
    if(post) setPostData(post)
  }, [post])

  const handleSubmit = (e) => {
    e.preventDefault()
    handleModalVisibility()
    currentId ?
      dispatch(updatePost(currentId, {...postData, name: currentUser?.result?.name})) :
      dispatch(createPost({...postData, name: currentUser?.result?.name}))
    clear()
  }

  const clear = () => {
    setCurrentId(null)
    setPostData({ title: "", message: "", tags: "", selectedFile: "",})
  }

  if(!currentUser?.result?.name) {
    return (
      <Paper style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
        <Typography variant="h6" align="center">
          You must be logged in to add your best memories !
        </Typography>
      </Paper>
    )
  }
  
  return (
    <Paper>
      <form
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
        autoComplete="off"
        noValidate
        onSubmit={handleSubmit}
      >
        <div style={{padding: '2rem'}}>
        <Typography variant="h6">{currentId ? 'Update' : 'Create'} your unique Memory</Typography>
        <TextField name="title" variant="outlined" label="Title" fullWidth value={postData.title} onChange={(e) => setPostData({...postData, title: e.target.value})}/>
        <TextField name="message" variant="outlined" label="Message" fullWidth value={postData.message} onChange={(e) => setPostData({...postData, message: e.target.value})}/>
        <TextField name="tags" variant="outlined" label="Tags" fullWidth value={postData.tags} onChange={(e) => setPostData({...postData, tags: e.target.value})}/>
        </div>
        <div style={{ width: '97%', margin: '10px 0' }}>
          <FileBase type='file' multiple={false} onDone={({ base64 }) => setPostData({ ...postData, selectedFile: base64 })} />
        </div>

        <Button style={{marginBottom: 10,  width: '300px',
          ['@media (max-width:600px)']: {
            width: 'auto',
          }, }} variant="contained" color="primary"  type="submit" > 
          {currentId ? 'Update' : 'Create'} the memory 
        </Button>
        <Button variant="contained" color="secondary" size='small' onClick={clear} style={{ width: '300px',
          ['@media (max-width:600px)']: {
            width: 'auto',
          } , marginBottom: 15}}> 
          Clear
        </Button>
      </form>
    </Paper>
  );
};

export default Form;
