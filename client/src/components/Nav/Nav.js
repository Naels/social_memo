import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { AppBar, Avatar, Button, Typography } from "@mui/material";
import memories from "../../images/memories.png";

import { useDispatch } from "react-redux";
import { LOGOUT } from "../../constants/actionTypes";

import decode from 'jwt-decode'

import { useNavigate, useLocation } from "react-router-dom";

const Nav = ( { handleModalVisibility }) => {
    const dispatch = useDispatch()
    const history = useNavigate()
    const location = useLocation()
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile'))) 

    useEffect(()=> {
      const token = user?.token
      if(token) {
        const decoded = decode(token)
        if(decoded.exp *1_000 < new Date().getTime())
          logout()
      }
      setUser(JSON.parse(localStorage.getItem('profile')))
    }, [location])


    const logout = () => {

      setUser(null)
      dispatch({type: LOGOUT})
      history('/')
      window.location.reload()
    }

  return (
    <AppBar
      sx={{
        borderRadius: 15,
        margin: "30px 0",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        flexWrap: 'wrap',
      }}
      position="static"
      color="inherit"
    >
      <div style={{ display: "flex", alignItems: "center" }}>
        <Typography component={Link} to="/" sx={{ color: "rgba(0,183,255, 1)", }} variant="h2" align="center">Memo ❤</Typography>
        <img sx={{ marginLeft: "15px", }} src={memories} alt="memories" height="60" />
      </div>

        {user ? (
          <>
            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100px' }}>
                <Avatar style={{color: '#FFF', backgroundColor: 'rgb(103, 58, 183)'}} alt={user.result.name} src={user.result.imageUrl}>{user.result.name.charAt(0)}</Avatar> 
            </div>
            <Button variant="contained" style={{borderRadius: '50%', height: '60px', width:'60px'}} color='primary' onClick={handleModalVisibility}>Add</Button>
            <Button variant="contained" style={{marginLeft: '1rem'}} color='primary' onClick={logout}>Logout</Button>
          </>
        ): (
          <Link to="/Auth">
          <Button variant="contained" color="primary">
            Log in
          </Button>
        </Link>
        )}

    </AppBar>
  );
};

export default Nav;
