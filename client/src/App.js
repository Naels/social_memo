import React from "react";
import { Container } from "@mui/material";

import { Routes, Route } from "react-router-dom";

import { BrowserRouter } from "react-router-dom";

import { useState } from "react";

import Nav from "./components/Nav/Nav";
import Auth from "./pages/Auth/Auth";
import Home from "./pages/Home/Home";

import './app.css'

const App = () => {
  const [displayModal, setDisplayModal] = useState(false);
  const handleModalVisibility = () => {
    setDisplayModal((prev) => !prev);
  };

  return (
    <BrowserRouter>
      <Container maxWidth="lg">
        <Nav handleModalVisibility={handleModalVisibility} />
      </Container>
      <Routes>
        <Route path="/" element={<Home handleModalVisibility={handleModalVisibility} displayModal={displayModal}/>}/>
        <Route path="/Auth" element={<Auth />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
