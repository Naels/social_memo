import React, {useState} from 'react'

import { Avatar, Paper, Grid, Button, Typography, Container } from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import InputForm from './InputForm'

import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import {signin, signup } from '../../actions/auth'

const initState = { firstName: '', lastName: '', email:'', password:'', confirmPassword:''}

const Auth = () => {
  const [showPassword, setShowPassword] = useState(false)
  const [isSignup, setIsSignUp] = useState(false)
  const [formData, setFormData] = useState(initState)
  const dispatch = useDispatch()
  const history = useNavigate()

  const switchMode = () => {
    setIsSignUp((prev) => !prev)
    setShowPassword(false)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if(isSignup){
      dispatch(signup(formData, history))
    }else{
      dispatch(signin(formData, history))
    }
  }

  const handleChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.value})
  }

  const handleShowPassword = () =>{
    setShowPassword((prev) => !prev)
  }

  return (
    <Container component='main' maxWidth='xs'>
      <Paper elevation={3} style={{marginTop: {spacing:8}, display: 'flex', flexDirection: 'column', alignItems: 'center', padding: {spacing:2}}}>
        <Avatar style={{ margin: {spacing:1},backgroundColor: "FFF"}}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography variant='h5'>{isSignup ? 'Sign Up' : 'Sign In'} </Typography>
        <form onSubmit={handleSubmit} style={{width: '80%', marginTop: {spacing:3}}}>
          <Grid container spacing={2}>
            {
              isSignup && (
                <>
                  <InputForm name="firstName" label="First Name" handleChange={handleChange} autoFocus half/>
                  <InputForm name="lastName" label="Lat Name" handleChange={handleChange} half/>
                </>
              )
            }
            <InputForm type="email" name="email" label="Email Address" handleChange={handleChange}/>
            <InputForm name="password" label="password" handleChange={handleChange} type={showPassword ? 'text' : 'password'} handleShowPassword={handleShowPassword}/>
            { isSignup && <InputForm name="confirmPassword" label="Repeat password" handleChange={handleChange} type={'password'} />}
          </Grid>
          <Button type='submit' fullWidth variant='contained' color='primary' style={{ marginTop: '1rem'}}>
            {isSignup ? 'Sign Up' : 'Sign In'}
          </Button>
          <Grid container justifyContent='center'>
            <Grid item>
              <Button onClick={switchMode} style={{ marginTop: '1rem'}}>
                {isSignup ? 'Already have an account ? Sign In' : 'Create an account'}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Container>
  )
}

export default Auth