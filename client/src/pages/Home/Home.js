import React, { useState, useEffect } from "react";

import { Container, Grow, Grid, Modal } from "@mui/material";
import Posts from "../../components/Posts/Posts";
import Form from "../../components/Form/Form";

import { useDispatch } from "react-redux";

import { getPosts } from "../../actions/posts";

const Home = ( { displayModal, handleModalVisibility }) => {
  const [currentId, setCurrentId] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPosts());
  }, [dispatch]);

  return (
    <Grow in>
      <Container>
        <Grid
          container
          justify="space-between"
          alignItems="stretch"
          spacing={3}
          sx={{
            flexDirection: {
              xs: "column-reverse",
              sm: "row",
              md: "row",
            },
          }}
        >
          <Grid item xs={12}>
            <Posts setCurrentId={setCurrentId} handleModalVisibility={handleModalVisibility} />
          </Grid>

          <Modal
            open={displayModal}
            onClose={() => {
              handleModalVisibility()
              setCurrentId(null)
            }}
            aria-labelledby="modal-title"
            aria-describedby="modal-description"
          >
            <div
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                width: "80%", 
                maxWidth: "400px", 
                bgcolor: "background.paper",
                border: "2px solid #000",
                boxShadow: 24,
                p: 4,
              }}
            >
              <Grid item xs={12}>
                <Form currentId={currentId} setCurrentId={setCurrentId} handleModalVisibility={handleModalVisibility} />
              </Grid>
            </div>
          </Modal>
        </Grid>
      </Container>
    </Grow>
  );
};

export default Home;
