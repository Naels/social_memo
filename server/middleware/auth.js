import jwt from "jsonwebtoken";

const auth = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decodedTokenData = jwt.verify(token, 'test')
        req.userId = decodedTokenData?.id
        next()
    } catch (error) {
        console.log(error)
    }
}

export default auth