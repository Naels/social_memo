import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

import User from '../models/user.js'


export const signin = async(req, res) => {
    const { email, password } = req.body
    try {
        const existingUser = await User.findOne({email})
        if(!existingUser) return res.status.json({message: "User doesn't exist"})

        const isPasswordCorrect = await bcrypt.compare(password, existingUser.password)
        if(!isPasswordCorrect) return res.status.json({message: "Invalid credentials"})

        const tokenJW = jwt.sign({email: existingUser.email, id: existingUser._id}, 'test', {expiresIn: "1h"})
        res.status(200).json({result: existingUser, tokenJW})
    } catch (error) {
        res.status(500).json({message: "Something went wrong"})
    }
}

export const signup = async(req, res) => {
    const { email, password, confirmPassword, firstName, lastName } = req.body

    try {
        const existingUser = await User.findOne({email})
        if(existingUser) return res.status.json({message: "Mail adress already use, try to connect !"})
    
        if(password !== confirmPassword) return res.status.json({message: "Password don't match"})
    
        const hash = await bcrypt.hash(password, 12)
        const create = await User.create({email, password: hash, name:`${firstName} ${lastName}`})
        const tokenJW = jwt.sign({email: create.email, id: create._id}, 'test', {expiresIn: "1h"})
        res.status(200).json({result: create, tokenJW}) 
        
    } catch (error) {
        res.status(500).json({message: "Something went wrong"})
    }
}

